class CreateImages < ActiveRecord::Migration
  def up
    create_table :images do |t|
      t.string :name, null: false
      t.references :user, null: false, index: true
      t.string :file, null: false
      t.timestamps null: false
    end
  end

  def down
    drop_table :images
  end
end
