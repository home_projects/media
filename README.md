# README #

### Тестовое задание ###

Реализовать медиа-коллекцию пользователя, состоящую из ссылок и картинок. 
Пользователь может войти, выйти, добавить/удалить что-то в коллекции и поделиться ссылкой на коллекцию. 
Приложение должно быть оттестировано, используя Rspec. 
Все непонятные моменты можно решить на свое усмотрение и не забыть указать это в README к проекту на Github

### Детали ###

* Для работы может понадобиться установка доп. библиотеки
  (sudo apt-get install libmagickwand-dev)
* Сделана суть, без наведения красоты, слайдеров и пр.
* Регистрация без подтверждения через почту
* Любой пользователь может посмотреть коллекцию другого пользователя без регистрации, по прямой ссылке. 
* Создавать, редактировать и удалять картинки может только залогиненный пользователь и только свои.