Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'

  devise_for :users

  resources :users, only: [] do
    resources :images, only: [:index]
  end

  resources :images, except: [:index]

end
