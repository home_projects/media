FactoryGirl.define do

  factory :user do
    sequence :email do |n|
      "user#{n}@domain.com"
    end
    password '11111111'
    password_confirmation '11111111'
  end

  factory :image do
    name 'Forest'
    user
    file { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'support', 'files', 'forest.jpg')) }
  end

end