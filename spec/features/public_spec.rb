require "rails_helper"
include TestHelpers

feature 'Public Access' do

  context 'Any user without session' do

    scenario 'can see any collection' do
      user = FactoryGirl.create(:user)
      FactoryGirl.create(:image, user: user)
      FactoryGirl.create(:image, user: user)

      visit user_images_path(user)

      expect(page.all("img#image-#{user.images.first.id}").length).to eq(1)
      expect(page.all("img#image-#{user.images.last.id}").length).to eq(1)
    end

    scenario 'can see any image through action Show' do
      user = FactoryGirl.create(:user)
      image = FactoryGirl.create(:image, user: user)

      visit image_path(image)

      expect(page.all("img#image-#{image.id}").length).to eq(1)
    end

  end

end
