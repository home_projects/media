require "rails_helper"
include TestHelpers

feature 'Images' do
  let!(:user) {sign_in FactoryGirl.create(:user)}

  context 'Signed-in user' do
    scenario 'user can see his own images' do
      FactoryGirl.create(:image, user: user)
      FactoryGirl.create(:image, user: user)

      visit user_images_path(user)

      expect(page.all("img#image-#{user.images.first.id}").length).to eq(1)
      expect(page.all("img#image-#{user.images.last.id}").length).to eq(1)
    end

    scenario 'user can see image through action Show' do
      image = FactoryGirl.create(:image, user: user)

      visit image_path(image)
      expect(page.all("img#image-#{user.images.first.id}").length).to eq(1)
    end

    scenario 'user can update image' do
      image = FactoryGirl.create(:image, user: user)

      visit edit_image_path(image)
      fill_in 'image_name', with: 'abc'
      click_on 'Update Image'

      expect(image.reload.name).to eq 'abc'
      expect(page).to have_content 'Image was successfully updated'
    end

    scenario 'user can destroy image' do
      image = FactoryGirl.create(:image, user: user)

      visit user_images_path(user)

      expect(image.in? user.images).to eq true

      find("#delete-#{image.id}").click

      expect(image.in? user.images).to eq false
      expect(page).to have_content 'Image was successfully destroyed'
    end
  end

end