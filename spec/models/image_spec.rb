require "rails_helper"

RSpec.describe Image, :type => :model do
  it "can be created" do
    user = FactoryGirl.create(:user)
    image = FactoryGirl.create(:image, user: user)
    expect(Image.first).to eq(image)
    expect(Image.first.user).to eq(user)
    expect(user.images).to eq([image])
  end
end