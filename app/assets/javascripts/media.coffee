Media = {}

$ ->
  Media.flash =
    hideMsg: ->
      alert = $('.alert')
      if alert.length > 0
        setTimeout ->
          alert.fadeOut 1000, ->
            alert.remove()
        , 3000

    init: ->
      @hideMsg()

  Media.flash.init()
