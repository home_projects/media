class Image < ActiveRecord::Base
  mount_uploader :file, FileUploader

  belongs_to :user

  validates :name, :file, presence: true

  def self.reprocess_images
    Image.find_each do |image|
      if image.file?
        image.file.recreate_versions!
        image.save!
      end
    end
  end
end
