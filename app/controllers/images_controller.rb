class ImagesController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index, :show]
  before_action :set_image, only: [:show, :edit, :update, :destroy]
  before_action :check_access, only: [:edit, :update, :destroy]



  # GET /images
  # GET /images.json
  def index
    @images = Image.where(user_id: params[:user_id].to_i)
    @it_is_my_images_list = current_user && (current_user.id == params[:user_id].to_i)
  end

  # GET /images/1
  # GET /images/1.json
  def show
  end

  # GET /images/new
  def new
    @image = Image.new
  end

  # GET /images/1/edit
  def edit
  end

  # POST /images
  # POST /images.json
  def create
    @image = Image.new(image_params)

    respond_to do |format|
      if @image.save
        format.html { redirect_to @image, notice: 'Image was successfully created.' }
        format.json { render :show, status: :created, location: @image }
      else
        format.html { render :new }
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /images/1
  # PATCH/PUT /images/1.json
  def update
    respond_to do |format|
      if @image.update(image_params)
        format.html { redirect_to @image, notice: 'Image was successfully updated.' }
        format.json { render :show, status: :ok, location: @image }
      else
        format.html { render :edit }
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /images/1
  # DELETE /images/1.json
  def destroy
    @image.destroy
    respond_to do |format|
      format.html { redirect_to user_images_path(current_user), notice: 'Image was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def check_access
    unless @image.user == current_user
      flash[:warning] = "You can edit only your own images!"
      redirect_to user_images_path(current_user)
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_image
      @image = Image.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def image_params
      hash = params.fetch(:image, {}).permit(:name, :file, :file_cache)
      hash['user_id'] = current_user.id
      hash
    end
end
